package com.rweqx.view;

import javafx.scene.control.Button;

public class MyButton extends Button {

    private final int BASE_WIDTH = 100;


    public MyButton(String s){
        super(s);
        this.setPrefWidth(BASE_WIDTH);

    }
    public MyButton(String s, int i){
        super(s);
        this.setPrefWidth(BASE_WIDTH * i + (i-1) * MyWindow.PADDING);

    }
}
