package com.rweqx.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import java.awt.*;
import java.io.File;
import java.util.List;

public class AFileFinder {
    public File showFileChooser() {
        DirectoryChooser DC = new DirectoryChooser();
        File selectedDir = DC.showDialog(new Stage());

        return selectedDir;

    }
}
