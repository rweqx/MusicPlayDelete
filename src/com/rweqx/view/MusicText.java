package com.rweqx.view;

import javafx.scene.control.Label;

public class MusicText extends Label {
    private String name;

    public String getName(){
        return name;
    }

    public MusicText(String s){
        super(s);
        name = s;
    }
}
