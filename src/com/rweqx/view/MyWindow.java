package com.rweqx.view;

import com.rweqx.controller.UIController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


import javafx.scene.control.ScrollPane;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MyWindow {

    public static final int PADDING = 10;

    private Stage window;

    private final int WIDTH = 500;
    private final int HEIGHT = 1000;


    /**
     * Builds and shows Window
     *
     */
    public void buildWindow(){
        //Build Basic Scene
        Scene scene = new Scene(new Group());
        window.setTitle("Music Play Delete");
        window.setWidth(WIDTH);
        window.setHeight(HEIGHT);

        MyButton play, pause, find, delete, skip;
        play = new MyButton("Play", 1);
        play.setOnAction(new PlayHandler());

        pause = new MyButton("Pause", 1);
        pause.setOnAction(new PauseHandler());
        find = new MyButton("Find", 2);
        find.setOnAction(new FindHandler());

        delete = new MyButton("Delete");
        delete.setOnAction(new DeleteHandler());

        skip = new MyButton("Skip");
        skip.setOnAction(new SkipHandler());


        GridPane pane = new GridPane();
        pane.setPadding(new Insets(PADDING, PADDING, PADDING, PADDING));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setAlignment(Pos.CENTER);

        pane.add(play, 0, 0, 1, 1);
        pane.add(pause, 1, 0, 1, 1);
        pane.add(find, 0, 1, 2, 1);
        pane.add(delete, 0, 2);
        pane.add(skip, 1, 2);

        listMusicBox = new VBox(10);

        ScrollPane scroll = new ScrollPane();
        scroll.setContent(listMusicBox);
        scroll.setMaxHeight(800);
        scroll.setMaxWidth(450);

        pane.add(scroll, 0, 3, 2, 1);

        ((Group) scene.getRoot()).getChildren().add(pane);


        window.setScene(scene);
    }

    VBox listMusicBox;


    public MyWindow(Stage window) {
        this.window = window;

    }
    UIController UIC;

    public void addSongsToBox(List<File> listOfFiles){
        System.out.println("Adding songs to box!");
        for(File f : listOfFiles){
            listMusicBox.getChildren().add(new MusicText(f.getName()));
        }
    }

    public void removeSong(File f){
        List remove = new ArrayList();
        String fileName = f.getName();
        for(Node n : listMusicBox.getChildren()){
            if(n instanceof MusicText){
                if(((MusicText)n).getName().equals(fileName)){
                    remove.add(n);
                }
            }
        }
        listMusicBox.getChildren().removeAll(remove);
    }
    public void giveController(UIController UIC){
        this.UIC = UIC;
    }


    public void show(){
        window.show();
    }



    public class PlayHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            UIC.playQueue();
        }
    }

    public class PauseHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            UIC.pauseQueue();
        }
    }

    public class SkipHandler implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent event) {
            UIC.skipCurrentSong();
        }
    }
    public class DeleteHandler implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent event) {
            UIC.deleteCurrentSong();
        }
    }

    public class FindHandler implements EventHandler<ActionEvent>{
        @Override
        public void handle(ActionEvent event) {
            UIC.openFindWindow();
        }
    }
}
