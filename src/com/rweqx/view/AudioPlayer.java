package com.rweqx.view;

import com.rweqx.controller.AudioController;
import com.rweqx.controller.UIController;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class AudioPlayer {
    AudioController AC;
    UIController UIC;

    File currentSong;
    MediaPlayer media;

    boolean playing;

    public AudioPlayer(){
        playing = false;
        media = null; //TODO ISN'T THIS REDUNDANT?
    }

    public void giveController(AudioController AC, UIController UIC){
        this.AC = AC;
        this.UIC = UIC;
    }

    public void playSong(File f) {

        if(f != null) {
            currentSong = f;
            System.out.println("Playing " + f.getPath());

            Media hit = new Media(f.toURI().toString());
            media = new MediaPlayer(hit);
            media.setOnEndOfMedia(() -> {
                endOfSong();
            });
            play();
        }
    }

    public void endOfSong(){
        if(media != null) {
            media.dispose();
            media = null;
        }
        playing = false;
        //Does cleanup of ending song stuff -> To implement with Queue stuff...
        AC.songEnded();
        UIC.songEnded(currentSong);
        currentSong = null;

        //Automatically attempts to play the next song.
        playSong(AC.getNextSong());
    }

    public void skipCurrentSong() {
        endOfSong();
    }

    public File getCurrentSong() {
        return currentSong;
    }

    public void play() {
        if(media != null && playing == false) {
            media.play();
            playing = true;
        }
    }

    public void pause(){
        if(playing) {
            media.pause();
            playing = false;
        }
    }

    public File earlyTerminateAndReturnSong() {
        if(media != null){
            media.dispose();
            playing = false;//Does cleanup of ending song stuff -> To implement with Queue stuff...
            AC.songEnded();
            UIC.songEnded(currentSong);
            return currentSong;
        }
        return null;
    }
}
