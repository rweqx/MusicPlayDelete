package com.rweqx.controller;

import com.rweqx.model.AudioFileFinder;
import com.rweqx.model.AudioQueue;
import com.rweqx.view.AudioPlayer;
import com.rweqx.view.MyWindow;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AudioController {

    MyWindow window;
    AudioFileFinder AFF;
    AudioQueue AQ;
    AudioPlayer AP;

    public AudioController(MyWindow window, AudioPlayer AP){
        AQ = new AudioQueue();
        AFF = new AudioFileFinder();

        this.AP = AP;
        this.window = window;
    }

    public List<File> getAndAddFilesInDirectoryToQueue(File dir){
        ArrayList<File> filesToAdd = (ArrayList<File>) AFF.findFilesInDirectory(dir);
        if(filesToAdd != null){
            AQ.addToQueue(filesToAdd);
        }
        return filesToAdd;
    }

    public void deleteCurrentSong(File f) {
        AQ.deleteSong(f);
    }

    public File getNextSong() {
        return AQ.nextSong();
    }

    public void songEnded() {
        // TODO - Somethign with the queue in the future...
    }
}
