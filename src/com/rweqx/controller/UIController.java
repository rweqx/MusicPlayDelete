package com.rweqx.controller;

import com.rweqx.view.AFileFinder;
import com.rweqx.view.AudioPlayer;
import com.rweqx.view.MyWindow;


import java.io.File;
import java.util.List;

public class UIController {
    MyWindow window;
    AudioController AC;
    AudioPlayer AP;


    public UIController(MyWindow window, AudioController AC, AudioPlayer AP){
        this.window = window;
        this.AC = AC;
        this.AP = AP;
    }


    public void playQueue() {
        if(AP.getCurrentSong() != null){
            AP.play();
        }else{
            AP.playSong(AC.getNextSong());
        }
    }

    public void pauseQueue() {
        AP.pause();

    }

    public void skipCurrentSong() {
        AP.skipCurrentSong();
    }

    public void deleteCurrentSong(){
        File f = AP.earlyTerminateAndReturnSong();
        AC.deleteCurrentSong(f);
        AP.playSong(AC.getNextSong());

    }

    public void openFindWindow() {
        AFileFinder AFF = new AFileFinder();
        File dir = AFF.showFileChooser();
        List<File> filesToAdd = AC.getAndAddFilesInDirectoryToQueue(dir);
        window.addSongsToBox(filesToAdd);
    }

    public void songEnded(File f) {
        window.removeSong(f);
    }
}

