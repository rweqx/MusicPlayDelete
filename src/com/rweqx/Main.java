package com.rweqx;

import com.rweqx.controller.AudioController;
import com.rweqx.controller.UIController;
import com.rweqx.view.AudioPlayer;
import com.rweqx.view.MyWindow;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Platform.setImplicitExit(true);

        MyWindow window = new MyWindow(primaryStage);
        AudioPlayer AP = new AudioPlayer();

        AudioController AC = new AudioController(window, AP);
        UIController UIC = new UIController(window, AC, AP);

        AP.giveController(AC, UIC);

        window.giveController(UIC);
        window.buildWindow();

        window.show();

    }

    public static void main(String args[]){
        launch(null);

    }
}
