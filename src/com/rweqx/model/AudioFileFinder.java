package com.rweqx.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AudioFileFinder {

    public AudioFileFinder(){

    }



    String[] extensions = {"mp3", "m4a", "flac"}; //TODO Add supported audio formats.

    /**
     * Returns list of files within the directory that have the supported extensions.
     * @param dir
     * @return
     */
    public List<File> findFilesInDirectory(File dir){
        if(dir != null) {
            if (dir.isDirectory()) {
                if(!dir.getName().equals("delete")) {
                    List listOfFiles = new ArrayList<File>();

                    //Find all audio files.

                    File files[] = dir.listFiles();
                    boolean added;
                    if (files.length > 0) {
                        for (File f : files) {
                            if (f.isDirectory()) {
                                List<File> someFiles = findFilesInDirectory(f); // Recursive add of subfolders.
                                if (someFiles != null) {
                                    listOfFiles.addAll(someFiles);
                                }
                            } else {
                                added = false;
                                for (String ext : extensions) {
                                    if (f.getName().contains(ext)) {
                                        listOfFiles.add(f);
                                    }
                                    added = true;
                                }
                                if (!added) {
                                    System.out.println("Could not add file with name " + f.getName());
                                }
                            }
                        }
                    }
                    return listOfFiles;
                }else{
                    System.out.println("Skip adding delete directory");
                }
            } else {
                System.out.println("Not a file directory lol");
            }
        }
        return null;
    }
}
