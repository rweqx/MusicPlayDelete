package com.rweqx.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds queue of audio files to play...
 *
 */
public class AudioQueue {
    public AudioQueue(){
        queuePosition = -1;
        queue = new ArrayList<File>();
        //played = new ArrayList<File>();

    }

    List<File> queue;
    List<File> played;
    int queuePosition;

    public File nextSong(){
        if(queue.size() > queuePosition +1) {
            queuePosition++;
            return getSong();
        }
        return null;
    }


    //TODO Not yet have buttons for.
    public File previousSong(){
        queuePosition --;
        return(getSong());
    }

    public void addToQueue(ArrayList<File> addMe){

        queue.addAll(addMe);
        for(File f : addMe){
            System.out.println("Files" + f.getPath());
        }

    }

    public File getSong(){
        if(queue.size() > queuePosition){
            if(queuePosition != -1){
                File f = (File)queue.get(queuePosition);
                System.out.println("Passing file to player: " + f.getName());
                return f;
            }
        }
        return null;
    }

    /*
    public void deleteSongFromQueue(File f){

        queue.remove(f);
    }
    */

    /**
     * Moves song to a folder "delete" in the same dir.
     * @param startFile
     *
     */
    public void deleteSong(File startFile) {

        String deleteDir = startFile.getParent().toString() + File.separator + "delete";
        String fileName = startFile.getName();
        int i = 0;

        File delFolder = new File(deleteDir);

        if(!delFolder.exists()) {
            delFolder.mkdirs();
        }

        String extendFileName = deleteDir + File.separator + fileName;
        while(new File(deleteDir + extendFileName).exists()){
            extendFileName = fileName + i;
            i++;
        }

        File endFile = new File(extendFileName);
        System.out.println("Moving " + fileName + " to " + extendFileName);

        if(startFile.renameTo(endFile)){
            System.out.println("Moved to delete folder successfully with method 1");
        }else{
            try {
                //Attempting move techinque number 2
                InputStream in = new FileInputStream(startFile);
                OutputStream out = new FileOutputStream(endFile);
                endFile.setWritable(true);

                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.flush();
                out.close();

                startFile.setWritable(true);
                File deleteMe = new File(startFile.getAbsolutePath());
                deleteMe.setWritable(true);

                if(deleteMe.delete()){

                    System.out.println("File deleted");
                }else{
                    System.out.println(deleteMe.exists());
                    System.out.println(deleteMe.canWrite());
                    deleteMe.deleteOnExit();
                    System.out.println("File only moved, not deleted");
                }

                System.out.println("Moved to delete folder successfully with method 2. ");


            }catch(Exception e){
                System.out.println("File could NOT be deleted");
            }
        }
    }
}
