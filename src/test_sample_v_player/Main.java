package test_sample_v_player;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Platform.setImplicitExit(true);

        String bip = "bip2.mp4";
        Media hit = new Media(new File(bip).toURI().toString());


        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setAutoPlay(true);

        MediaView mediaView = new MediaView(mediaPlayer);

        Group root = new Group();


        scene = new Scene(root, 1000, 500);
        ((Group) scene.getRoot()).getChildren().add(mediaView);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);


        stage = primaryStage;
        stage.setResizable(true);

        stage.setFullScreen(true);
        mediaPlayer.play();
        mediaView.setPreserveRatio(true);
        primaryStage.show();


    }
    Stage stage;
    Scene scene;

    public static void main(String[] args) {
        launch(args);
    }
}
